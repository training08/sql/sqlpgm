-- Trigger after insert
create trigger training08.first_trigger after insert on training08.table_1 
for each row 
insert into training08.table_1_audit 
 SET action = 'INSERT',
     name = new.first_name,
     email = new.email,
     parent_id = new.id;

-- Trigger after update
create trigger training08.first_trigger_upd after update on training08.table_1 
for each row 
insert into training08.table_1_audit 
 SET action = 'UPDATE',
     name = new.first_name,
     email = new.email,
     parent_id = new.id;
     
-- Trigger after delete	
create trigger training08.first_trigger_DELETE after delete on training08.table_1 
for each row 
insert into training08.table_1_audit 
 SET action = 'DELETE',
     name = OLD.first_name,
     email = OLD.email,
     parent_id = OLD.id+'a';

drop trigger training08.first_trigger;
drop trigger training08.first_trigger_upd;
drop trigger training08.first_trigger_DELETE;
